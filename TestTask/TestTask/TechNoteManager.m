//
//  TechNoteManager.m
//  TestTask
//
//  Created by Oleg on 23.03.15.
//  Copyright (c) 2015 Oleg. All rights reserved.
//

#import "TechNoteManager.h"

@implementation TechNoteManager
{
    NSMutableArray *techNotesData;
}

@synthesize delegate;


#pragma mark Constructor

- (id)initWithPlistHardcodedData:(NSString *)plistFileName
{
    self = [super init];
    if(self) {
        NSString *filePath = [[NSBundle mainBundle] pathForResource:plistFileName ofType:@"plist"];
        
        techNotesData = [[NSMutableArray alloc] init];
        
        
        for (NSDictionary * dict in [NSArray arrayWithContentsOfFile:filePath]) {
            [self addTechNote:[dict objectForKey:@"text"]  timestamp:[dict objectForKey:@"timestamp"]];
        }
        
    }
    return self;
}

-(void)addNewItem:(NSString *)text
{
    [self addTechNote:text timestamp:[NSDate date]];
    [self groupDataByDate];
}

-(void)addTechNote:(NSString*)text timestamp:(NSDate *)timestamp
{
    TechNote *techNote = [[TechNote alloc] initWithData:timestamp text:text techNoteId:[techNotesData count]];
    [techNotesData addObject:techNote];
}

-(void)deleteItemWithIndex:(int)itemId
{ 
    for (TechNote *techNote in techNotesData) {
        if (techNote.techNoteId == itemId) {
            techNote.removed = YES;
        }
    }
    [self groupDataByDate];
}

-(void)getGrouppedData
{
    [self groupDataByDate];
}

-(void)groupDataByDate
{
    NSMutableDictionary *sections = [[NSMutableDictionary alloc] init];
    
    BOOL found;
  
    NSSortDescriptor *sortDescriptor;
    sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timestamp"
                                                 ascending:NO];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray;
    sortedArray = [techNotesData sortedArrayUsingDescriptors:sortDescriptors];
    
    NSMutableArray *orderedKeys;
    orderedKeys = [[NSMutableArray alloc] init];
    
    [orderedKeys addObject:kItemToAddTemplate];
    [sections setObject:[[NSMutableArray alloc] init] forKey:kItemToAddTemplate];
    
    for (TechNote *item in sortedArray)
    {
        if (item.removed) {
            continue;
        }
        NSString *date= [item.timestamp convertDateToStringForSection];
    
        found = NO;
        for (NSString *str in [sections allKeys])
        {
            if ([str isEqualToString:date])
            {
                found = YES;
            }
        }
        if (!found)
        {
            [orderedKeys addObject:date];
            [sections setObject:[[NSMutableArray alloc] init] forKey:date];
        }
    }
    
    [[sections objectForKey:kItemToAddTemplate] addObject:[NSNumber numberWithInt:0]];
    
    for (TechNote *item in sortedArray)
    {
        if (!item.removed) {
             [[sections objectForKey:[item.timestamp convertDateToStringForSection]] addObject:item];
        }
       
    }
    
    for (NSString *key in [sections allKeys])
    {
        [[sections objectForKey:key] sortUsingDescriptors:[NSArray arrayWithObject:[NSSortDescriptor sortDescriptorWithKey:@"timestamp" ascending:YES]]];
    }

    [self.delegate techNoteDataUpdatedForTable:sections sectionsTitle:orderedKeys];
    
}



@end
