//
//  GlobalDataSingleton.m
//  OnlinePay
//
//  Created by gelgard on 12.08.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import "GlobalDataSingleton.h"


@implementation GlobalDataSingleton 
{
    TechNoteManager *techNoteManager;
}




static GlobalDataSingleton* _sharedGlobalData = nil;

+ (instancetype)messageWithString:(NSString *)message
{
    return [GlobalDataSingleton sharedGlobalData];
}

+(GlobalDataSingleton*)sharedGlobalData
{
	@synchronized([GlobalDataSingleton class])
	{
		if (!_sharedGlobalData)
			_sharedGlobalData = [[GlobalDataSingleton alloc]init];
        
		return _sharedGlobalData;
	}
    
	return nil;
}

+(id)alloc
{
	@synchronized([GlobalDataSingleton class])
	{
		NSAssert(_sharedGlobalData == nil, @"Attempted to allocate a second instance of a singleton.");
		_sharedGlobalData = [super alloc];
        
        
        
		return _sharedGlobalData;
	}
    
	return nil;
}

-(id)init {
	self = [super init];
	if (self != nil) {
        techNoteManager = [[TechNoteManager alloc] initWithPlistHardcodedData:kPlistFileName];
       	}
    
	return self;
}

-(TechNoteManager *)getTechNoteManager
{
    return techNoteManager;
}

@end
