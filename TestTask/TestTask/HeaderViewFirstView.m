//
//  HeaderViewFirstView.m
//  TestTask
//
//  Created by Oleg on 24.03.15.
//  Copyright (c) 2015 Oleg. All rights reserved.
//

#import "HeaderViewFirstView.h"

@implementation HeaderViewFirstView
{
    IBOutlet UILabel *titleLabel;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
    [titleLabel setText:NSLocalizedString(@"newItem", nil)];
}


@end
