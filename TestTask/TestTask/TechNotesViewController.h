//
//  TechNotesViewController.h
//  TestTask
//
//  Created by Oleg on 23.03.15.
//  Copyright (c) 2015 Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TechNotesViewController : UIViewController <TechNoteManagerDelegate>

-(IBAction)closeModalView:(id)sender;
-(IBAction)addItem:(id)sender;

@end
