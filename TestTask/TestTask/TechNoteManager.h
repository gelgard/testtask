//
//  TechNoteManager.h
//  TestTask
//
//  Created by Oleg on 23.03.15.
//  Copyright (c) 2015 Oleg. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TechNote.h"

@protocol TechNoteManagerDelegate
@optional
-(void)techNoteDataUpdatedForTable:(NSMutableDictionary *)sectionsInfo  sectionsTitle:(NSArray*)sectionsTitle;

@end

@interface TechNoteManager : NSObject
{
    id<TechNoteManagerDelegate> delegate;
}

@property (strong) id delegate;

- (id)initWithPlistHardcodedData:(NSString *)plistFileName;
-(void)getGrouppedData;
-(void)deleteItemWithIndex:(int)itemIndex;
-(void)addNewItem:(NSString *)text;


@end
