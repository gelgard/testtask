//
//  TechNote.h
//  TestTask
//
//  Created by Oleg on 23.03.15.
//  Copyright (c) 2015 Oleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TechNote : NSObject

@property (nonatomic,strong) NSString *text;
@property (nonatomic,strong) NSDate *timestamp;
@property BOOL removed;
@property int techNoteId;


- (id)initWithData:(NSDate *)timestamp text:(NSString *)text techNoteId:(int)techNoteId;

@end
