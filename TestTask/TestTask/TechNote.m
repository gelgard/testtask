//
//  TechNote.m
//  TestTask
//
//  Created by Oleg on 23.03.15.
//  Copyright (c) 2015 Oleg. All rights reserved.
//

#import "TechNote.h"

@implementation TechNote
{
    
}

#pragma mark Constructor

- (id)initWithData:(NSDate *)timestamp text:(NSString *)text techNoteId:(int)techNoteId
{
    self = [super init];
    if(self) {
        self.timestamp = timestamp;
        self.text = text;
        self.removed = NO;
        self.techNoteId = techNoteId;

    }
    return self;
}

@end
