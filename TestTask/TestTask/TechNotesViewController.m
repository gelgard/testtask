//
//  TechNotesViewController.m
//  TestTask
//
//  Created by Oleg on 23.03.15.
//  Copyright (c) 2015 Oleg. All rights reserved.
//

#import "TechNotesViewController.h"
#import "TechNoteTableViewCell.h"
#import "TechNoteManager.h"
#import "HeaderView.h"
#import <QuartzCore/QuartzCore.h>
#import "HeaderViewtechNote.h"
#import "HeaderViewFirstView.h"


@interface TechNotesViewController () <TechNoteCellEditProcessing>
{
    NSDictionary *groupedTechNoteData;
    NSArray *sectionsTitlesOrdered;
    IBOutlet UILabel *titleLabel;
    IBOutlet UIButton *closeBtn;
    IBOutlet UIButton *saveBtn;
    IBOutlet UIView *contentView;
    IBOutlet NSLayoutConstraint *topTableMargin;
    IBOutlet UIView *topView;
    CGRect initTableFrame;
    int keyboardHeight;
}

@property (nonatomic, strong) TechNoteTableViewCell *techNoteCell;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation TechNotesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [[GlobalDataSingleton sharedGlobalData] getTechNoteManager].delegate = self;
    [[[GlobalDataSingleton sharedGlobalData] getTechNoteManager] getGrouppedData];
    
    [self initingElements];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Keyboard Observer


- (void)keyboardWillShow:(NSNotification *)sender
{
    CGSize keyboardSize = [[[sender userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    keyboardHeight = keyboardSize.height;
    initTableFrame = self.tableView.frame;
}

- (void)keyboardWillHide:(NSNotification *)sender
{
    
    [self.tableView setFrame:initTableFrame];
}

#pragma mark - Initing Element
-(void)initingElements
{
    [titleLabel setText:NSLocalizedString(@"modalTitle", nil)];
    [closeBtn setTitle:NSLocalizedString(@"modalCloseBtn", nil) forState:UIControlStateNormal];
    [saveBtn setTitle:NSLocalizedString(@"modalSaveBtn", nil) forState:UIControlStateNormal];
    contentView.clipsToBounds = YES;
    contentView.layer.cornerRadius = 5.0f;
}

#pragma mark - Control Action
-(IBAction)addItem:(id)sender
{
    TechNoteTableViewCell *cell = (TechNoteTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [[[GlobalDataSingleton sharedGlobalData] getTechNoteManager] addNewItem:cell.textArea.text];
}


-(IBAction)closeModalView:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - TechNoteCellEditProcessing

-(void)TechNoteCellNowEding:(UITableViewCell *)editingCell
{
    CGRect rectOfCellInTableView = [self.tableView rectForRowAtIndexPath:[self.tableView indexPathForCell:editingCell]];
    CGRect rectOfCellInSuperview = [self.tableView convertRect:rectOfCellInTableView toView:[self.tableView superview]];
    
    int bottomCelPosition, keyboardYOrigin,deltaCellBottomWithKeyboard;
    bottomCelPosition = rectOfCellInSuperview.origin.y+topView.frame.size.height + rectOfCellInTableView.size.height-20;
    keyboardYOrigin = self.view.frame.size.height - keyboardHeight;
    deltaCellBottomWithKeyboard = bottomCelPosition - keyboardYOrigin+topView.frame.size.height;

    if (bottomCelPosition>keyboardYOrigin) {
        CGRect currentTableFrame;
        currentTableFrame = self.tableView.frame;
        currentTableFrame.origin.y=currentTableFrame.origin.y-deltaCellBottomWithKeyboard;
        [self.tableView setFrame:currentTableFrame];

    }
    
    
}

#pragma mark - TechNoteManagerDelegate
-(void)techNoteDataUpdatedForTable:(NSMutableDictionary *)sectionsInfo sectionsTitle:(NSArray *)sectionsTitle
{
    groupedTechNoteData = [NSDictionary dictionaryWithDictionary:sectionsInfo];
    sectionsTitlesOrdered = [NSArray arrayWithArray:sectionsTitle];
    [self.tableView reloadData];
   
}

#pragma mark - techNoteCell
- (TechNoteTableViewCell *)techNoteCell
{
    if (!_techNoteCell) {
        _techNoteCell = [self.tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TechNoteTableViewCell class])];
    }
    
    return _techNoteCell;
}

#pragma mark - Configure
- (void)configureCell:(TechNoteTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    if (indexPath.section>0) {
        TechNote *item = [[groupedTechNoteData objectForKey:[sectionsTitlesOrdered objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
        
        cell.textArea.text = item.text;
        [cell.dateLabel  setText:[item.timestamp convertDateToStringInRow]];
        cell.techNoteId = item.techNoteId;
        cell.delegate = self;
    } else
    {
        [cell showAsNewItem];
    }
    
    
}

#pragma mark - TableView Delegate & Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return  [sectionsTitlesOrdered count];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView;
    CGRect headerViewFrame =CGRectMake(0, 0, tableView.frame.size.width, 18);
    if (section>0) {
         headerView = [[HeaderViewtechNote alloc] initWithFrame:headerViewFrame];
        [((HeaderViewtechNote *)headerView).dayLabel setText:[NSString stringWithFormat:@"Day %i",section]];
        NSString *sectionHeader = [sectionsTitlesOrdered objectAtIndex:section];
        [((HeaderViewtechNote *)headerView).dateLabel setText:[NSString stringWithFormat:@"(%@)",sectionHeader]];
    } else
    {
        headerView = [[HeaderViewFirstView alloc] initWithFrame:headerViewFrame];
    }
        
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int rowCount;
    rowCount = [[groupedTechNoteData objectForKey:[sectionsTitlesOrdered objectAtIndex:section]] count];
    
    return rowCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TechNoteTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([TechNoteTableViewCell class])];
    
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{

    if (IS_IOS8_OR_ABOVE) {
        return UITableViewAutomaticDimension;
    }
    
    [self configureCell:self.techNoteCell forRowAtIndexPath:indexPath];
    
    [self.techNoteCell updateConstraintsIfNeeded];
    [self.techNoteCell layoutIfNeeded];
    
    return [self.techNoteCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
}


#pragma mark - Recognizers
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
