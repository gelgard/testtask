//
//  TechNoteTableViewCell.m
//  TestTask
//
//  Created by Oleg on 23.03.15.
//  Copyright (c) 2015 Oleg. All rights reserved.
//

#import "TechNoteTableViewCell.h"

@implementation TechNoteTableViewCell
{
    IBOutlet UIButton *deleteButton;
    IBOutlet NSLayoutConstraint *labelHeight;
}

@synthesize delegate;
- (void)setBounds:(CGRect)bounds
{
    [super setBounds:bounds];    
    self.contentView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    [self.contentView updateConstraintsIfNeeded];
    [self.contentView layoutIfNeeded];
    
}

#pragma mark - Text View Delegate
-(void)textViewDidBeginEditing:(UITextView *)textView
{
    [[self delegate] TechNoteCellNowEding:self];
}

-(void)textViewDidChange:(UITextView *)textView
{
    UITableView *tableView = (UITableView *)self.superview.superview;
    [tableView beginUpdates];
    [tableView endUpdates];
    [[self delegate] TechNoteCellNowEding:self];
}

#pragma mark - Recognizers
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

#pragma mark - Control Action

-(IBAction)deleteItem:(id)sender
{
    [[[GlobalDataSingleton sharedGlobalData] getTechNoteManager] deleteItemWithIndex:self.techNoteId];
}

-(void)showAsNewItem
{
    [deleteButton setAlpha:0];
    labelHeight.constant =0;
    self.textArea.text = @"";
    self.delegate = nil;
    [self.contentView layoutIfNeeded];
    
}

@end
