//
//  HeaderViewtechNote.h
//  TestTask
//
//  Created by Oleg on 24.03.15.
//  Copyright (c) 2015 Oleg. All rights reserved.
//

#import "HeaderView.h"

@interface HeaderViewtechNote : HeaderView

@property (nonatomic, strong) IBOutlet UILabel *dayLabel;
@property (nonatomic, strong) IBOutlet UILabel *dateLabel;
@end
