//
//  GlobalDataSingleton.h
//  OnlinePay
//
//  Created by gelgard on 12.08.14.
//  Copyright (c) 2014 uran. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TechNoteManager.h"


@interface GlobalDataSingleton : NSObject
{
    
    
}

+(GlobalDataSingleton*)sharedGlobalData;

-(TechNoteManager *)getTechNoteManager;


@end
