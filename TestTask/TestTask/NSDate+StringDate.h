//
//  NSDate+StringDate.h
//  TestTask
//
//  Created by Oleg on 23.03.15.
//  Copyright (c) 2015 Oleg. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (StringDate)

-(NSString *) convertDateToStringForSection;
-(NSString *) convertDateToStringInRow;

@end
