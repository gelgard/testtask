//
//  HeaderView.h
//  TestTask
//
//  Created by Oleg on 23.03.15.
//  Copyright (c) 2015 Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HeaderView : UIView

@property (nonatomic, weak) IBOutlet UIView *view;

@end
