//
//  TechNoteTableViewCell.h
//  TestTask
//
//  Created by Oleg on 23.03.15.
//  Copyright (c) 2015 Oleg. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TechNoteCellEditProcessing
@optional
-(void)TechNoteCellNowEding:(UITableViewCell *)editingCell;

@end

@interface TechNoteTableViewCell : UITableViewCell<UITextViewDelegate>
{
     id<TechNoteCellEditProcessing> delegate;
}

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UITextView *textArea;
@property int techNoteId;
@property (strong) id delegate;


-(IBAction)deleteItem:(id)sender;
-(void)showAsNewItem;

@end
