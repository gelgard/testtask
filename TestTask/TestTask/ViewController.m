//
//  ViewController.m
//  TestTask
//
//  Created by Oleg on 23.03.15.
//  Copyright (c) 2015 Oleg. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    [self initingElements];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark Initing Elements
-(void)initingElements
{
    [showModalViewBtn setTitle:NSLocalizedString(@"showModalViewBtn", nil) forState:UIControlStateNormal];
    [[GlobalDataSingleton sharedGlobalData] getTechNoteManager];
}

@end
