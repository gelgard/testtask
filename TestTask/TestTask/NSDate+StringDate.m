//
//  NSDate+StringDate.m
//  TestTask
//
//  Created by Oleg on 23.03.15.
//  Copyright (c) 2015 Oleg. All rights reserved.
//

#import "NSDate+StringDate.h"

@implementation NSDate (StringDate)

-(NSString *) convertDateToStringForSection {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MMM-yyyy"];
    return [formatter stringFromDate:self]; }

-(NSString *) convertDateToStringInRow {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MMM-yyyy HH:mm:ss"];
    return [formatter stringFromDate:self]; }

@end
